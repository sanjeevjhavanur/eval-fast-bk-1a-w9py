using System.ComponentModel.DataAnnotations;

namespace FortCode.Dtos
{
    public class BarCreate
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string FavoriteDrinkName { get; set; }
        [Required]
        public int CityId { get; set; }
    }
}