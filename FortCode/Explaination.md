Startup File:

1. Added AddDbContext<FortContext>() along with the connection string that can be retrieved from appsettings.json file.
2. Added Dependency Injection definitions for the Repositories
3. Added the set up for the BasicAuthentication
4. Added app.UseAuthentication(); app.UseAuthorization(); for the app level user authentication
5. Enabled CORS by applying the services.AddCors(); and app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
6. Startup constructor was missing IConfiguration and added it to set the Configuration property.
