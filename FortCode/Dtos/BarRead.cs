namespace FortCode.Dtos
{
    public class BarRead
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FavoriteDrinkName { get; set; }
        public int CityId { get; set; }
    }
}
