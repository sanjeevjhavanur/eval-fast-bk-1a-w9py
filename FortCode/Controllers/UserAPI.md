1. Added UserController to achieve the user authentication.
2. UserController contructor takes the parameter IUserRepository which gets injected during the runtime by Dependency Injection. This helps to achieve loose coupling.
3. All the CRUD logic is encapsulated within the UserRepository to achieve separation of concern design pattern.
4. The controller is decorated with [Authorize] attribute to ensure only authenticated users have the access to the API methods.
5. The Authenticate function uses [AllowAnonymous] attribute(which overrides [Authorize] attribute) to allow anyone to call this function for Authentication.
6. Data Transfer Objects(DTOs) are used in API function so that the underlying DB entities are not exposed.
7. Approprite HTTP responses are rendered for validation error or any system failures.