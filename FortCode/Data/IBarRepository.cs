using FortCode.Dtos;
using FortCode.Models;
using System;
using System.Collections.Generic;

namespace FortCode.Data
{
    public interface IBarRepository
    {
        IEnumerable<BarRead> Get();
        IEnumerable<SharedBarRead> GetSharedBar(int id);
        BarRead Add(BarCreate Bar);
        string Delete(int id);
    }
}