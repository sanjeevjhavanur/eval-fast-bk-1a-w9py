using FortCode.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FortCode.Controllers;
using FortCode.Data;
using Moq;
using AutoFixture;
using FortCode.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace FortCodeTests
{
    [TestClass]
    public class CityControllerTests
    {
        private CityController _cityController;
        private Mock<ICityRepository> _mockCityRepository;
        private Fixture _fixture;
        private IEnumerable<CityRead> _cities;

        [TestInitialize]
        public void Initialize()
        {
            _mockCityRepository = new Mock<ICityRepository>();
            _cityController = new CityController(_mockCityRepository.Object);
            _fixture = new Fixture();
            _cities = _fixture.CreateMany<CityRead>(5);
        }

        [TestMethod]
        public void Get_Returns_OkResult_On_Success()
        {
            //Arrange

            _mockCityRepository.Setup(t => t.Get()).Returns(_cities);

            //Act
            var result = _cityController.Get();
            var okResult = result.Result as OkObjectResult;

            //Assert
            _mockCityRepository.Verify(a => a.Get(), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_OkResult_On_Success()
        {
            //Arrange
            var city = _fixture.Create<CityCreate>();
            var cityRead = _fixture.Create<CityRead>();
            _mockCityRepository.Setup(t => t.Add(city)).Returns(cityRead);

            //Act
            var result = _cityController.Post(city);
            var okResult = result.Result as OkObjectResult;

            //Assert
            _mockCityRepository.Verify(a => a.Add(city), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_City_Is_Null()
        {
            //Arrange
            CityCreate city = null;
            var cityRead = _fixture.Create<CityRead>();
            _mockCityRepository.Setup(t => t.Add(city)).Returns(cityRead);

            //Act
            var result = _cityController.Post(city);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockCityRepository.Verify(a => a.Add(city), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_City_Name_Is_Null()
        {
            //Arrange
            CityCreate city = _fixture.Create<CityCreate>();
            city.Name = null;
            var cityRead = _fixture.Create<CityRead>();
            _mockCityRepository.Setup(t => t.Add(city)).Returns(cityRead);

            //Act
            var result = _cityController.Post(city);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockCityRepository.Verify(a => a.Add(city), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_Country_Is_Null()
        {
            //Arrange
            CityCreate city = _fixture.Create<CityCreate>();
            city.Country = null;
            var cityRead = _fixture.Create<CityRead>();
            _mockCityRepository.Setup(t => t.Add(city)).Returns(cityRead);

            //Act
            var result = _cityController.Post(city);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockCityRepository.Verify(a => a.Add(city), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_UserId_Is_Zero()
        {
            //Arrange
            CityCreate city = _fixture.Create<CityCreate>();
            city.UserId = 0;
            var cityRead = _fixture.Create<CityRead>();
            _mockCityRepository.Setup(t => t.Add(city)).Returns(cityRead);

            //Act
            var result = _cityController.Post(city);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockCityRepository.Verify(a => a.Add(city), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Delete_Returns_OkResult_On_Success()
        {
            //Arrange
            int cityId = 10;
            _mockCityRepository.Setup(t => t.Delete(cityId)).Returns("Successfully Removed the record.");

            //Act
            var result = _cityController.Delete(cityId);
            var okResult = result as OkObjectResult;

            //Assert
            _mockCityRepository.Verify(a => a.Delete(cityId), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Delete_Returns_BadRequest_When_CityId_Is_Zero()
        {
            //Arrange
            int cityId = 0;
            _mockCityRepository.Setup(t => t.Delete(cityId)).Returns("Successfully Removed the record.");

            //Act
            var result = _cityController.Delete(cityId);
            var okResult = result as BadRequestResult;

            //Assert
            _mockCityRepository.Verify(a => a.Delete(cityId), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }
    }
}
