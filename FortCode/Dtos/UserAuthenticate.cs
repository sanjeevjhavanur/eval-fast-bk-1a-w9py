using System.ComponentModel.DataAnnotations;

namespace FortCode.Dtos
{
    public class UserAuthenticate
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}