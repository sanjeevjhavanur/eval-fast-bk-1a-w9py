using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Dtos
{

    public class CityCreate
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public int UserId { get; set; }
        
    }
}