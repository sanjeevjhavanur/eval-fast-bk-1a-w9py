using System.Collections.Generic;
using System.Linq;
using FortCode.Models;
using FortCode.Dtos;

namespace FortCode.Helpers
{
    public static class ExtensionMethods
    {
        public static IEnumerable<UserAccountRead> WithoutPasswords(this IEnumerable<User> users) {
            return users.Select(x => x.WithoutPassword());
        }

        public static UserAccountRead WithoutPassword(this User user) {
            user.Password = null;
            return new UserAccountRead
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email
            };
        }
    }
}