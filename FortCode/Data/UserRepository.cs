﻿
using FortCode.Dtos;
using FortCode.Models;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Helpers;

namespace FortCode.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly FortContext _context;

        public UserRepository(FortContext context)
        {
            _context = context;
        }

        public UserAccountRead Add(UserAccountCreate user)
        {
            var existingUser = _context.Users.FirstOrDefault(u => u.Email.ToLower().Trim() == user.Email.ToLower().Trim());
            if (existingUser != null)
            {
                return new UserAccountRead
                {
                    Id = existingUser.Id,
                    Name = existingUser.Name,
                    Email = existingUser.Email
                };
            }

            var newUser = new User
            {
                Name = user.Name,
                Email = user.Email,
                Password = user.Password
            };

            _context.Users.Add(newUser);
            _context.SaveChanges();

            return new UserAccountRead
                {
                    Id = newUser.Id,
                    Name = newUser.Name,
                    Email = newUser.Email
                };
        }

        public UserAccountRead Get(int id)
        {
            return _context.Users.Where(u => u.Id == id).Select(u => new UserAccountRead
            {
                Id = u.Id,
                Name = u.Name,
                Email = u.Email

            }).FirstOrDefault();
        }

        public async Task<UserAccountRead> Authenticate(string username, string password)
        {
            var user = await Task.Run(() => _context.Users.SingleOrDefault(x => x.Name == username && x.Password == password));
            // return null if user not found
            if (user == null)
            {
                 return null;
            }

            // authentication successful so return user details without password
            return user.WithoutPassword();
        }

    }
}
