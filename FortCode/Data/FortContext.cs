﻿using System;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data
{
    public class FortContext : DbContext
    {
        public FortContext(DbContextOptions<FortContext> options): base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Bar> Bars { get; set; }
    }
}
