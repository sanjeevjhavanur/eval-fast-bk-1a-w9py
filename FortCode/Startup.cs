using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using FortCode.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using FortCode.Helpers;

namespace FortCode
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<FortContext>(
                opt => opt.UseSqlServer(Configuration.GetConnectionString("FortDbConnection"))
            ); //TODO: Change the connection string name
            services.AddMvc();
            services.AddCors();
            services.AddControllers();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IBarRepository, BarRepository>();

            // set basic authentication
            services
                .AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>(
                    "BasicAuthentication",
                    null
                );
        }

        public virtual void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            ILoggerFactory loggerFactory
        )
        {
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseFileServer()
                .UseRouting()
                .UseEndpoints(
                    endPoints =>
                    {
                        endPoints.MapControllers();
                    }
                );

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
