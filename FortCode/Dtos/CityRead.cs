using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Dtos
{
    public class CityRead
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public int UserId { get; set; }
        
    }
}