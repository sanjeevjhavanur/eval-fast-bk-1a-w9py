
using FortCode.Dtos;
using FortCode.Models;
using System.Linq;
using System.Collections.Generic;
using System;

namespace FortCode.Data
{
    public class BarRepository : IBarRepository
    {
        private readonly FortContext _context;

        public BarRepository(FortContext context)
        {
            _context = context;
        }

        public BarRead Add(BarCreate Bar)
        {
            var existingBar = _context.Bars.FirstOrDefault(u => u.Name.ToLower().Trim() == Bar.Name.ToLower().Trim() 
                                                    && u.FavoriteDrinkName.ToLower().Trim() == Bar.FavoriteDrinkName.ToLower().Trim()
                                                    && u.CityId == Bar.CityId);
            if (existingBar != null)
            {
                return new BarRead
                {
                    Id = existingBar.Id,
                    Name = existingBar.Name,
                    FavoriteDrinkName = existingBar.FavoriteDrinkName,
                    CityId = existingBar.CityId
                };
            }

            var newBar = new Bar
            {
                Name = Bar.Name,
                FavoriteDrinkName = Bar.FavoriteDrinkName,
                CityId = Bar.CityId
            };

            _context.Bars.Add(newBar);
            _context.SaveChanges();

            return new BarRead
                {
                   Id = newBar.Id,
                    Name = newBar.Name,
                    FavoriteDrinkName = newBar.FavoriteDrinkName,
                    CityId = newBar.CityId
                };
        }

        public IEnumerable<BarRead> Get()
        {
            return _context.Bars.Select(u => new BarRead
            {
                Id = u.Id,
                Name = u.Name,
                FavoriteDrinkName = u.FavoriteDrinkName
            }).AsEnumerable();
        }


        public IEnumerable<SharedBarRead> GetSharedBar(int id)
        {
            var sharedBars = from u in _context.Users 
            join c in _context.Cities on u.Id equals c.UserId 
            join b in _context.Bars on c.Id equals b.CityId 
            where b.Id == id 
            group u by new {b.Name, u.Id} into g
            select new SharedBarRead
            {
                BarName = g.Key.Name,
                SharedUserCount =  g.Count()
            };

            return sharedBars.AsEnumerable();
        }


        public string Delete(int id)
        {
            var existingBar = _context.Bars.FirstOrDefault(u => u.Id == id);
                                                   
            if (existingBar != null)
            {
                throw new Exception("The Bar not found in the system");
            }

            _context.Bars.Remove(existingBar);
            _context.SaveChanges();

            return "Successfully remove the Bar.";
        }

    }
}
