﻿
using FortCode.Dtos;
using FortCode.Models;
using System.Threading.Tasks;

namespace FortCode.Data
{
    public interface IUserRepository
    {
        
        UserAccountRead Get(int id);
        UserAccountRead Add(UserAccountCreate user);
        Task<UserAccountRead> Authenticate(string username, string password);
    }
}
