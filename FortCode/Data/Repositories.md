1. I have used Repository design pattern for code re-usability.
2. Each repository takes FortContext object of type DbContext via dependency injection.
3. Used Entity Framework Code first approach to add the DB migrations.
4. Effectively used Linq queries to perform any Linq to Sql queries.
5. All the repository functions are exposed via respective Interfaces so that the Controllers are implemented to the abstractions rather than actual implementations.