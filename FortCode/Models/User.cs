﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{

    public class User
    {
        public User()
        {
            Cities = new HashSet<City>();
        }
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        public ICollection<City> Cities { get; set; }
    }
}