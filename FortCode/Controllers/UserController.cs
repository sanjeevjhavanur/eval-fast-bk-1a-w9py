﻿using System;
using FortCode.Data;
using FortCode.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/User")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] UserAuthenticate userAuthenticate)
        {
            var user = _userRepository.Authenticate(userAuthenticate.Username, userAuthenticate.Password);

            if (user == null)
            {
                return BadRequest(new { message = "Invalid Username or password." });
            }

            return Ok(user);
        }

        [HttpGet("{id}")]
        public ActionResult<UserAccountRead> Get(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest();
                }
                return Ok(_userRepository.Get(id));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public ActionResult<UserAccountRead> Post([FromBody] UserAccountCreate user)
        {
            try
            {
                if (
                    user == null
                    || string.IsNullOrWhiteSpace(user.Email)
                    || string.IsNullOrWhiteSpace(user.Password)
                    || string.IsNullOrWhiteSpace(user.Name)
                )
                {
                    return BadRequest();
                }

                return Ok(_userRepository.Add(user));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
