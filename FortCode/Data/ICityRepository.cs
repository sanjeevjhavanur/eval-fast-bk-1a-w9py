using FortCode.Dtos;
using FortCode.Models;
using System;
using System.Collections.Generic;

namespace FortCode.Data
{
    public interface ICityRepository
    {
        IEnumerable<CityRead> Get();
        CityRead Add(CityCreate city);
        string Delete(int id);
    }
}