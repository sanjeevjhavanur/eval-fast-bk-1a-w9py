
using FortCode.Dtos;
using FortCode.Models;
using System.Linq;
using System.Collections.Generic;
using System;

namespace FortCode.Data
{
    public class CityRepository : ICityRepository
    {
        private readonly FortContext _context;

        public CityRepository(FortContext context)
        {
            _context = context;
        }

        public CityRead Add(CityCreate city)
        {
            var existingCity = _context.Cities.FirstOrDefault(u => u.Name.ToLower().Trim() == city.Name.ToLower().Trim() 
                                                    && u.Country.ToLower().Trim() == city.Country.ToLower().Trim()
                                                    && u.UserId == city.UserId);
            if (existingCity != null)
            {
                return new CityRead
                {
                    Id = existingCity.Id,
                    Name = existingCity.Name,
                    Country = existingCity.Country,
                    UserId = existingCity.UserId
                };
            }

            var newCity = new City
            {
                Name = city.Name,
                Country = city.Country,
                UserId = city.UserId
            };

            _context.Cities.Add(newCity);
            _context.SaveChanges();

            return new CityRead
                {
                    Id = newCity.Id,
                    Name = newCity.Name,
                    Country = newCity.Country,
                    UserId = newCity.UserId
                };
        }

        public IEnumerable<CityRead> Get()
        {
            return _context.Cities.Select(u => new CityRead
            {
                Id = u.Id,
                Name = u.Name,
                Country = u.Country,
                UserId = u.UserId
            }).AsEnumerable();
        }

        public string Delete(int id)
        {
            var existingCity = _context.Cities.FirstOrDefault(u => u.Id == id);
                                                   
            if (existingCity != null)
            {
                throw new Exception("The City not found in the system");
            }

            _context.Cities.Remove(existingCity);
            _context.SaveChanges();

            return "Successfully remove the City.";
        }

    }
}
