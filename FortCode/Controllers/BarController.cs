﻿using System;
using FortCode.Data;
using FortCode.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/Bar")]
    public class BarController : ControllerBase
    {
        private readonly IBarRepository _barRepository;

        public BarController(IBarRepository barRepository)
        {
            _barRepository = barRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<BarRead>> Get()
        {
            try
            {
                return Ok(_barRepository.Get());
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<SharedBarRead>> GetSharedBar(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest();
                }
                return Ok(_barRepository.GetSharedBar(id));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public ActionResult<BarRead> Post([FromBody] BarCreate bar)
        {
            try
            {
                if (
                    bar == null
                    || string.IsNullOrWhiteSpace(bar.Name)
                    || string.IsNullOrWhiteSpace(bar.FavoriteDrinkName)
                    || bar.CityId <= 0
                )
                {
                    return BadRequest();
                }

                return Ok(_barRepository.Add(bar));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest();
                }

                return Ok(_barRepository.Delete(id));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
