using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{
    public class Bar
    {
        public Bar()
        {
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string FavoriteDrinkName { get; set; }

        [Required]
        public int CityId { get; set; }
        public City City { get; set; }
    }
}
