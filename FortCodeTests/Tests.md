1. Used MOQ and AutoFixture libraries to fake the data
2. Unit tests are separated into each file per controller to test the API method functionalities.
3. Unit tests ensure thar proper set is done for each repository functions and Asserting the expected results.
4. To run the tests, navigate to the FortCodeTestsfolder and  use command - 'dotnet test'
