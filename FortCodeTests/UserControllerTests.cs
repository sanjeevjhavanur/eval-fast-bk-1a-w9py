using FortCode.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FortCode.Controllers;
using FortCode.Data;
using Moq;
using AutoFixture;
using FortCode.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;

namespace FortCodeTests
{
    [TestClass]
    public class UserControllerTests
    {
        private UserController _userController;
        private Mock<IUserRepository> _mockUserRepository;
        private Fixture _fixture;
        private UserAccountRead _userAccountRead;

        [TestInitialize]
        public void Initialize()
        {
            _mockUserRepository = new Mock<IUserRepository>();
            _userController = new UserController(_mockUserRepository.Object);
            _fixture = new Fixture();
            _userAccountRead = _fixture.Create<UserAccountRead>();
        }

        [TestMethod]
        public async Task Authenticate_Returns_OkResult_On_Success()
        {
            //Arrange
            var userAuthenticate = _fixture.Create<UserAuthenticate>();
            _mockUserRepository.Setup(t => t.Authenticate(userAuthenticate.Username, userAuthenticate.Password)).ReturnsAsync(_userAccountRead);

            //Act
            var result = await _userController.Authenticate(userAuthenticate);
            var okResult = result as OkObjectResult;

            //Assert
            _mockUserRepository.Verify(a => a.Authenticate(userAuthenticate.Username, userAuthenticate.Password), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Get_Returns_OkResult_On_Success()
        {
            //Arrange
            var userId = 100;
            _mockUserRepository.Setup(t => t.Get(It.IsAny<int>())).Returns(_userAccountRead);

            //Act
            var result =  _userController.Get(userId);
            var okResult = result.Result as OkObjectResult;
            
            //Assert
            _mockUserRepository.Verify(a => a.Get(userId), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Get_Returns_BadRequest_When_Id_Is_Zero()
        {
            //Arrange
            var userId = 0;
            _mockUserRepository.Setup(t => t.Get(It.IsAny<int>())).Returns(_userAccountRead);

            //Act
            var result =  _userController.Get(userId);
            var okResult = result.Result as BadRequestResult;
           
            //Assert
            _mockUserRepository.Verify(a => a.Get(userId), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Get_Returns_InternalServerError_When_Exception_Is_Thrown()
        {
            //Arrange
            var userId = 100;
            _mockUserRepository.Setup(t => t.Get(It.IsAny<int>())).Throws(new Exception());

            //Act
            var result =  _userController.Get(userId);
                      
            //Assert
            _mockUserRepository.Verify(a => a.Get(userId), Times.Once);
            Assert.IsNotNull(result);  
        }

        [TestMethod]
        public void Post_Returns_OkResult_On_Success()
        {
            //Arrange
            var user = _fixture.Create<UserAccountCreate>();
            _mockUserRepository.Setup(t => t.Add(user)).Returns(_userAccountRead);

            //Act
            var result =  _userController.Post(user);
            var okResult = result.Result as OkObjectResult;
            
            //Assert
            _mockUserRepository.Verify(a => a.Add(user), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_User_Is_Null()
        {
            //Arrange
            UserAccountCreate user = null;
            _mockUserRepository.Setup(t => t.Add(user)).Returns(_userAccountRead);

            //Act
            var result =  _userController.Post(user);
            var okResult = result.Result as BadRequestResult;
            
            //Assert
            _mockUserRepository.Verify(a => a.Add(user), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_User_Email_Is_Null()
        {
            //Arrange
            UserAccountCreate user = _fixture.Create<UserAccountCreate>();
            user.Email = null;
            _mockUserRepository.Setup(t => t.Add(user)).Returns(_userAccountRead);

            //Act
            var result =  _userController.Post(user);
            var okResult = result.Result as BadRequestResult;
            
            //Assert
            _mockUserRepository.Verify(a => a.Add(user), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_User_Password_Is_Null()
        {
            //Arrange
            UserAccountCreate user = _fixture.Create<UserAccountCreate>();
            user.Password = null;
            _mockUserRepository.Setup(t => t.Add(user)).Returns(_userAccountRead);

            //Act
            var result =  _userController.Post(user);
            var okResult = result.Result as BadRequestResult;
            
            //Assert
            _mockUserRepository.Verify(a => a.Add(user), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_User_Name_Is_Null()
        {
            //Arrange
            UserAccountCreate user = _fixture.Create<UserAccountCreate>();
            user.Name = null;
            _mockUserRepository.Setup(t => t.Add(user)).Returns(_userAccountRead);

            //Act
            var result =  _userController.Post(user);
            var okResult = result.Result as BadRequestResult;
            
            //Assert
            _mockUserRepository.Verify(a => a.Add(user), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }
    }
}
