1. Added CityController for performing City data CRUD operations
2. CityController contructor takes the parameter ICityRepository which gets injected during the runtime by Dependency Injection. This helps to achieve loose coupling.
3. All the CRUD logic is encapsulated within the CityRepository to achieve separation of concern design pattern.
4. The controller is decorated with [Authorize] attribute to ensure only authenticated users have the access to the API methods.
6. Data Transfer Objects(DTOs) are used in API function so that the underlying DB entities are not exposed.
7. Approprite HTTP responses are rendered for validation error or any system failures.
8. Get function returns IEnumerable<CityRead> so that query is deferred and clients can perform ODATA style queries.