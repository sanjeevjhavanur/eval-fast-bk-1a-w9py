﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{

    public class City
    {
        public City()
        {
            Bars = new HashSet<Bar>();
        }
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Bar> Bars { get; set; }
    }
}