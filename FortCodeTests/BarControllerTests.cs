using FortCode.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FortCode.Controllers;
using FortCode.Data;
using Moq;
using AutoFixture;
using FortCode.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace FortCodeTests
{
    [TestClass]
    public class BarControllerTests
    {
        private BarController _barController;
        private Mock<IBarRepository> _mockBarRepository;
        private Fixture _fixture;
        private IEnumerable<BarRead> _bars;

        [TestInitialize]
        public void Initialize()
        {
            _mockBarRepository = new Mock<IBarRepository>();
            _barController = new BarController(_mockBarRepository.Object);
            _fixture = new Fixture();
            _bars = _fixture.CreateMany<BarRead>(5);
        }

        [TestMethod]
        public void Get_Returns_OkResult_On_Success()
        {
            //Arrange

            _mockBarRepository.Setup(t => t.Get()).Returns(_bars);

            //Act
            var result = _barController.Get();
            var okResult = result.Result as OkObjectResult;

            //Assert
            _mockBarRepository.Verify(a => a.Get(), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_OkResult_On_Success()
        {
            //Arrange
            var bar = _fixture.Create<BarCreate>();
            var barRead = _fixture.Create<BarRead>();
            _mockBarRepository.Setup(t => t.Add(bar)).Returns(barRead);

            //Act
            var result = _barController.Post(bar);
            var okResult = result.Result as OkObjectResult;

            //Assert
            _mockBarRepository.Verify(a => a.Add(bar), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_Bar_Is_Null()
        {
            //Arrange
            BarCreate bar = null;
            var BarRead = _fixture.Create<BarRead>();
            _mockBarRepository.Setup(t => t.Add(bar)).Returns(BarRead);

            //Act
            var result = _barController.Post(bar);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockBarRepository.Verify(a => a.Add(bar), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_Bar_Name_Is_Null()
        {
            //Arrange
            BarCreate bar = _fixture.Create<BarCreate>();
            bar.Name = null;
            var BarRead = _fixture.Create<BarRead>();
            _mockBarRepository.Setup(t => t.Add(bar)).Returns(BarRead);

            //Act
            var result = _barController.Post(bar);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockBarRepository.Verify(a => a.Add(bar), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_FavoriteDrinkName_Is_Null()
        {
            //Arrange
            BarCreate bar = _fixture.Create<BarCreate>();
            bar.FavoriteDrinkName = null;
            var BarRead = _fixture.Create<BarRead>();
            _mockBarRepository.Setup(t => t.Add(bar)).Returns(BarRead);

            //Act
            var result = _barController.Post(bar);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockBarRepository.Verify(a => a.Add(bar), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Post_Returns_BadRequest_When_CityId_Is_Zero()
        {
            //Arrange
            BarCreate bar = _fixture.Create<BarCreate>();
            bar.CityId = 0;
            var BarRead = _fixture.Create<BarRead>();
            _mockBarRepository.Setup(t => t.Add(bar)).Returns(BarRead);

            //Act
            var result = _barController.Post(bar);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockBarRepository.Verify(a => a.Add(bar), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }

        [TestMethod]
        public void Delete_Returns_OkResult_On_Success()
        {
            //Arrange
            int barId = 10;
            _mockBarRepository.Setup(t => t.Delete(barId)).Returns("Successfully Removed the record.");

            //Act
            var result = _barController.Delete(barId);
            var okResult = result as OkObjectResult;

            //Assert
            _mockBarRepository.Verify(a => a.Delete(barId), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void Delete_Returns_BadRequest_When_BarId_Is_Zero()
        {
            //Arrange
            int barId = 0;
            _mockBarRepository.Setup(t => t.Delete(barId)).Returns("Successfully Removed the record.");

            //Act
            var result = _barController.Delete(barId);
            var okResult = result as BadRequestResult;

            //Assert
            _mockBarRepository.Verify(a => a.Delete(barId), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }


        [TestMethod]
        public void GetSharedBar_Returns_OkResult_On_Success()
        {
            //Arrange
            int barId = 10;
            var sharedBars = _fixture.CreateMany<SharedBarRead>(5);
            _mockBarRepository.Setup(t => t.GetSharedBar(barId)).Returns(sharedBars);

            //Act
            var result = _barController.GetSharedBar(barId);
            var okResult = result.Result as OkObjectResult;

            //Assert
            _mockBarRepository.Verify(a => a.GetSharedBar(barId), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public void GetSharedBar_Returns_BadRequest_When_BarId_Is_Zero()
        {
            //Arrange
            int barId = 0;
            var sharedBars = _fixture.CreateMany<SharedBarRead>(5);
            _mockBarRepository.Setup(t => t.GetSharedBar(barId)).Returns(sharedBars);

            //Act
            var result = _barController.GetSharedBar(barId);
            var okResult = result.Result as BadRequestResult;

            //Assert
            _mockBarRepository.Verify(a => a.GetSharedBar(barId), Times.Never);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, okResult.StatusCode);
        }
    }
}
