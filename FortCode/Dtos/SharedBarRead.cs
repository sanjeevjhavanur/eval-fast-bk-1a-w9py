namespace FortCode.Dtos
{
    public class SharedBarRead
    {
        public string BarName { get; set; }
        public int SharedUserCount { get; set; }
    }
}
