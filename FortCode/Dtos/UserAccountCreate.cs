using System.ComponentModel.DataAnnotations;

namespace FortCode.Dtos
{
    public class UserAccountCreate
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        
    }
}