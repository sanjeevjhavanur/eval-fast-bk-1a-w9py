﻿using System;
using FortCode.Data;
using FortCode.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/City")]
    public class CityController : ControllerBase
    {
        private readonly ICityRepository _cityRepository;

        public CityController(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }


        [HttpGet]
        public ActionResult<IEnumerable<CityRead>> Get()
        {
            try
            {
                return Ok(_cityRepository.Get());
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [HttpPost]
        public ActionResult<CityRead> Post([FromBody] CityCreate city)
        {
            try
            {
                if (city == null || string.IsNullOrWhiteSpace(city.Name)
                    || string.IsNullOrWhiteSpace(city.Country) || city.UserId <=0)
                {
                    return BadRequest();
                }

                return Ok(_cityRepository.Add(city));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest();
                }

                return Ok(_cityRepository.Delete(id));
            }
            catch (Exception ex)
            {
                //log exception message here
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
